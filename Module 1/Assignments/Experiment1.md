# Aim:

To study the characteristics of Linear Variable Differential Transducer

# Apparatus:

* Micrometer Screw Guage
* Primary and Secondary Winding
* Power Supply

# Theory:

 Principal of *Mutual Inductance* is used in __LVDT__

 # Observation Table

 |  Distance    |   Voltage |
 |--------------|-----------|
 |     0        |     0     |
 |    12.5      |    286    |
 |    13        |    225    |
 |    15        |    409    |

 # Procedure:

 1. First zero is set on micrometer and then reading is taken on indicator.
 2. One complete rotation of thimble corresponds to 0.5mm distance.
 3. Hence, for 1mm distance corresponding distance is reached.
 4. Plot graph using distance and voltage.