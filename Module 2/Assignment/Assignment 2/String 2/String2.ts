function strfn1()
{
    var a : HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var b : string = a.value;
    var c : string;
    var arr : string[] =[];

    document.getElementById("display").innerHTML += "<b>The original String is : </b>" + b;

    c = b.toUpperCase();
    document.getElementById("display").innerHTML += "</br><b>b.toUpperCase() : </b>" + c;

    c = b.toLowerCase();
    document.getElementById("display").innerHTML += "</br><b>b.toLowerCase() : </b>" + c;

    c = b.charAt(3);
    document.getElementById("display").innerHTML += "</br><b>b.charAt(3) : </b>" + c;

    arr = b.split(" ");
    document.getElementById("display").innerHTML += "</br><b>b.split(\" \") : </b>" + arr[0];
    document.getElementById("display").innerHTML += "</br><b>b.split(\" \") : </b>" + arr[1];
    document.getElementById("display").innerHTML += "</br><b>b.split(\" \") : </b>" + arr[2];
    document.getElementById("display").innerHTML += "</br><b>b.split(\" \") : </b>" + arr[3];
}